 
package kafka.producer;



import java.util.Properties;
import java.util.concurrent.ExecutionException;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.serialization.LongSerializer;

@Path("Producer/")
public class Producer {
	
	private final static String TOPIC = "Common_Data_Pool";
    private final static String BOOTSTRAP_SERVERS =
            "localhost:9092,localhost:9093,localhost:9094";
    private static KafkaProducer<Long, String> producer = null;
	private final static Properties props = new Properties();
	
	static {
	    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
	    				BOOTSTRAP_SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
		             LongSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
		         StringSerializer.class.getName());
		
		producer = new KafkaProducer<Long, String>(props);
	}
    public Producer() {
        // TODO Auto-generated constructor stub
    	if(producer == null) {
    		producer = new KafkaProducer<Long, String>(props);
    	}
    }

    @GET
	@Path("tester/")
	@Produces(MediaType.TEXT_PLAIN)
	public String tester() {
    	return "Producer is deployed";
    }
    
	@POST
	@Path("produce/")
	@Consumes("text/plain")
	@Produces(MediaType.TEXT_PLAIN)
	public String produce(String payload) {
		String ret = "";
		long time = System.currentTimeMillis();
		final ProducerRecord<Long, String> record =
                new ProducerRecord<>(TOPIC, time,
                            			payload);
		  try {
				RecordMetadata metadata = producer.send(record).get();
				
				long elapsedTime = System.currentTimeMillis() - time;
				
				System.out.printf("sent record(key=%s value=%s) " +
		                "meta(partition=%d, offset=%d) time=%d\n",
		        record.key(), record.value(), metadata.partition(),
		        metadata.offset(), elapsedTime);
				
				ret = "sent record(key="+record.key()+" value="+record.value()+") " +
			                "meta(partition="+metadata.partition()+", offset="+metadata.offset()+") time="+elapsedTime+"\n";
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				ret = e.toString();
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				ret = e.toString();
				e.printStackTrace();
			}
		return ret;
	}
}