 
package kafka.consumer;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;


@Path("Consumer")
public class Consumer {
	/**
     * Default constructor. 
     */
    public Consumer() {
        // TODO Auto-generated constructor stub
    }


    /**
     * Retrieves representation of an instance of Consumer
     * @return an instance of String
     */
	@GET
	@Produces("text/plain")
	public String tester() { 
		// TODO Auto-generated method stub
		return "Consumer is deployed";
//		throw new UnsupportedOperationException();
	}

	/**
     * PUT method for updating or creating an instance of Consumer
     * @content content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
	@PUT
	@Consumes("text/plain")
	public void resourceMethodPUT(String content) { 
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}
}