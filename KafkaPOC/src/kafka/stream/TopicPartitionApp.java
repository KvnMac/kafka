/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kafka.stream;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;

import util.XMLValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.function.Predicate;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Demonstrates, using the high-level KStream DSL, how to implement the WordCount program
 * that computes a simple word occurrence histogram from an input text.
 * <p>
 * In this example, the input stream reads from a topic named "streams-plaintext-input", where the values of messages
 * represent lines of text; and the histogram output is written to topic "streams-wordcount-output" where each record
 * is an updated count of a single word.
 * <p>
 * Before running this example you must create the input topic and the output topic (e.g. via
 * {@code bin/kafka-topics.sh --create ...}), and write some data to the input topic (e.g. via
 * {@code bin/kafka-console-producer.sh}). Otherwise you won't see any data arriving in the output topic.
 */
@Path("TopicPartitionApp/")
public class TopicPartitionApp {
  public static boolean doExit = false;
  
  private final static String SOURCE_TOPIC = "Common_Data_Pool";
    private final static String BOOTSTRAP_SERVERS =
            "localhost:9092,localhost:9093,localhost:9094";
  private final static Properties props = new Properties();
  static {
    props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-partition");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Long().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
  }
  
  @GET
  @Path("test/")
  public String test() {
    return "TopicPartitionApp is deployed";
  }
  
  @GET
  @Path("shutdownTopicPartitionApp/")
  public String shutdownTopicPartitionApp() {
    doExit = true;
    return "doExit changed to TRUE";
  }
  
  @GET
  @Path("resetTopicPartitionApp/")
  public String resetTopicPartitionApp() {
    doExit = false;
    return "doExit changed to false";
  }
  
  public static void main(String[] args) {
	  runTopicPartitionAppConsole();
  }
  
  public static void runTopicPartitionAppConsole() {
	  final StreamsBuilder builder = new StreamsBuilder();

      final KStream<String, String> source = builder.stream(SOURCE_TOPIC);
      
      source.filter((key,payload) -> 
				true
		  	).to("Unknown_Topic");

      final KafkaStreams streams = new KafkaStreams(builder.build(), props);

      streams.start();
      
  }
  
  
  @POST
  @Path("runTopicPartitionApp/")
  @Produces(MediaType.TEXT_PLAIN)
  public String runTopicPartitionApp() {
    new Thread() {
      @Override
      public void run() {
        final StreamsBuilder builder = new StreamsBuilder();

            final KStream<Long, String> source = builder.stream(SOURCE_TOPIC);
            
            filterKStream(source);
//            source.filter((key,payload) -> true).to("Unknown_Topic");
//            source.filter(new MyPredicate2()).to("output-topic-2");
//            source.filter(new MyPredicate3()).to("output-topic-3");

//            final KTable<String, Long> counts = source
//                .flatMapValues(value -> Arrays.asList(value.toLowerCase(Locale.getDefault()).split(" ")))
//                .groupBy((key, value) -> value)
//                .count();

            // need to override value serde to Long type
//            counts.toStream().to("streams-wordcount-output", Produced.with(Serdes.String(), Serdes.Long()));

            final KafkaStreams streams = new KafkaStreams(builder.build(), props);
//            final CountDownLatch latch = new CountDownLatch(1);
            streams.start();
            
				/*
				 * try { streams.start(); new Thread("streams-TopicPartitionApp-exit") {
				 * 
				 * @Override public void run() { if(doExit) { streams.close();
				 * latch.countDown(); doExit = false; } } }.start(); latch.await(); } catch
				 * (final Throwable e) {
				 * System.out.println("TopicPartitionApp Exited with error"); System.exit(1); }
				 * System.out.println("TopicPartitionApp Exited with grace"); System.exit(0);
				 * 
				 */      }
//
    }.start();
            
    return "TopicPartitionApp has started";
  }
  
  
	private void filterKStream(KStream<Long, String> source) {
		// TODO Auto-generated method stub
	   XMLValidator xmlValidator = new XMLValidator(); 
	   Predicate<String> predicate_fom = str -> xmlValidator.isFomToGtmPayload(str);
 	  
//	   Predicate<String> temp = str -> true;
// 	   List<Predicate<String>> pList = new ArrayList<Predicate<String>>();
// 	   pList.add(predicate_fom);
// 	   for(Predicate<String> p:pList) {
// 		  temp = p.negate().and(temp);
// 	   }
// 	  
// 	   final Predicate<String> predicate_unknown = temp;
 	   
 	  @SuppressWarnings("unchecked")
 	  KStream<Long, String>[] branches = source.branch(
 			    (key, value) -> predicate_fom.test(value), /* first predicate  */
// 			    (key, value) -> key.startsWith("B"), /* second predicate */
 			    (key, value) -> true                 /* third predicate  */
 			  );
 	  
 	 branches[0].to("FOM_TOPIC");
 	 branches[1].to("Unknown_Topic");
 	  
// 	   if(predicate_fom.test(payload))return true;
// 	   return predicate_fom.negate().and(predicate_unknown).test(payload);

//       source.filter((key,payload) -> 
//						predicate_unknown.test(payload)
//    		   		).to("Unknown_Topic");
       
//        source.filter((key,payload) -> 
//        					predicate_fom.test(payload)
//        			  ).to("FOM_TOPIC");
        

	}
  
    /*public static void main(final String[] args) {
        
        

        // setting offset reset to earliest so that we can re-run the demo code with the same pre-loaded data
        // Note: To re-run the demo, you need to use the offset reset tool:
        // https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Streams+Application+Reset+Tool
//        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        final StreamsBuilder builder = new StreamsBuilder();

        final KStream<String, String> source = builder.stream("streams-plaintext-input");

        final KTable<String, Long> counts = source
            .flatMapValues(value -> Arrays.asList(value.toLowerCase(Locale.getDefault()).split(" ")))
            .groupBy((key, value) -> value)
            .count();

        // need to override value serde to Long type
        counts.toStream().to("streams-wordcount-output", Produced.with(Serdes.String(), Serdes.Long()));

        final KafkaStreams streams = new KafkaStreams(builder.build(), props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-wordcount-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (final Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }*/
}