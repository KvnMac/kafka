package util;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

public class XMLValidator {
	DocumentBuilderFactory dbFactory = null;
	DocumentBuilder dBuilder = null;
	
	
	public XMLValidator(){
		dbFactory = DocumentBuilderFactory.newInstance();
        try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    public boolean validate(String payload, String flowName) {
    	
    	   Predicate<String> predicate_fom = str -> isFomToGtmPayload(str);
//    	   Predicate<String> predicate_partyMerge = str -> false;
    	   Predicate<String> predicate_unknown = str -> true;
    	   
    	   List<Predicate<String>> pList = new ArrayList<Predicate<String>>();
    	   pList.add(predicate_fom);
    	   for(Predicate<String> p:pList) {
    		   predicate_unknown = p.negate().and(predicate_unknown);
    	   }
    	   
    	   
    	   if(predicate_fom.test(payload))return true;
    	   return predicate_fom.negate().and(predicate_unknown).test(payload);
    	   
    }
    
    public boolean isFomToGtmPayload(String str) {
    	Document doc = null;
		try {
			doc = dBuilder.parse(new ByteArrayInputStream(str.getBytes(Charset.forName("UTF-8"))));
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		doc.getDocumentElement().normalize();
        Element rootElement = doc.getDocumentElement();
        NodeList headerElement =  doc.getElementsByTagName("env:Header");
        NodeList bodyElement =  doc.getElementsByTagName("env:Body");
        NodeList headerTLElement = doc.getElementsByTagName("headerTL");
        if(rootElement == null || headerElement.getLength()!=1 || bodyElement.getLength()!=1 || headerTLElement.getLength()!=1)
        	return false;
//        System.out.print("Root element: ");
//        System.out.println(rootElement.getNodeName());
//        System.out.println(headerElement.item(0).getNodeName());
//        System.out.println(bodyElement.item(0).getNodeName());
//        System.out.println(headerTLElement.item(0).getNodeName());
		return true;

    }
}
